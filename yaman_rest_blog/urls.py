from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import TemplateView

from rest_framework import permissions  # new
from rest_framework.schemas import get_schema_view

# from drf_yasg.views import get_schema_view  # new
# from drf_yasg import openapi  # new

# schema_view = get_schema_view(  # new
#     openapi.Info(
#         title="Blog API",
#         default_version="v1",
#         description="A sample API for learning DRF",
#         terms_of_service="https://www.google.com/policies/terms/",
#         contact=openapi.Contact(email="hello@example.com"),
#         license=openapi.License(name="BSD License"),
#     ),
#     public=True,
#     permission_classes=(permissions.AllowAny,),
# )


def bad(request):
    """ Simulates a server error """
    1 / 0


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
]


urlpatterns += i18n_patterns(
    path("bad/", bad),
    path("", include("djvue.urls")),
    path(f"{settings.ADMIN_URL}/", admin.site.urls),
    path("user/", include("user.urls", namespace="user")),
    path("api/v1/", include("user.api.urls", namespace="user_api")),
    path("api/app/", include("posts.urls")),
    path("api-auth/", include("rest_framework.urls")),
    path("api/app/dj-rest-auth/", include("dj_rest_auth.urls")),
    path(
        "api/app/dj-rest-auth/registration/", include("dj_rest_auth.registration.urls")
    ),
    path(
        "openapi",
        get_schema_view(  # new
            title="Blog API",
            description="A sample API for learning DRF",
            version="1.0.0",
        ),
        name="openapi-schema",
    ),
    # path(
    #     "swagger/",
    #     schema_view.with_ui("swagger", cache_timeout=0),
    #     name="schema-swagger-ui",
    # ),
    # path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc",),
)

if settings.DEBUG:
    urlpatterns += [
        # Testing 404 and 500 error pages
        path("404/", TemplateView.as_view(template_name="404.html"), name="404"),
        path("500/", TemplateView.as_view(template_name="500.html"), name="500"),
    ]

    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar

    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
